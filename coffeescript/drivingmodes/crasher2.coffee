
class Crasher2
  constructor: (@race, @oracle) ->
    @crashed = false
    @step = 0

  getThrottle: ->
    myCar = @race.myCar
    position = myCar.getLastPosition()

    throttle = if position.prev? then position.prev.throttle else 0

    if position.prev? and position.prev.crash
      @race.slippingAngle = if @crashed then Math.max(@race.slippingAngle, Math.abs(position.prev.angle)) else Math.abs(position.prev.angle)
      console.log "slipping angle = #{@race.slippingAngle}"
      @crashed = true
    else if position.prev? and position.prev.waiting

    else if position.prev? and position.prev.postCrash

    else

      if not @crashed
        throttle = 0.1 + (2*position.piece.trackOffset / @race.track.length)
      else
        @step += 1
        if @step > 5
          @step = 0
          throttle = 1
          targetPiece = position.piece.next.next
          while targetPiece isnt position.piece.prev
            targetThrottle = 0
            throttleLow = 0
            throttleHigh = 1
            distance = @race.track.calculatePieceDistance(position.piece, targetPiece, position.lane)
            distance -= position.x
            distance += targetPiece.getLaneLength(position.lane.distanceFromCenter)
            targetSpeed = @getTargetSpeed(targetPiece, position.lane)
            while throttleHigh - throttleLow > 0.05
              targetThrottle = (throttleLow + throttleHigh) / 2
              finalSpeed = @getFinalSpeed(distance, position.speed, position.acceleration, targetThrottle)
              if finalSpeed > targetSpeed
                throttleHigh = targetThrottle
              else
                throttleLow = targetThrottle
            throttle = Math.min(throttle, targetThrottle)
            targetPiece = targetPiece.prev
          throttle = @getAverageThrottle(position, throttle, 5)

    throttle = if throttle > 1 then 1 else throttle
    throttle = if throttle < 0 then 0 else throttle

    return throttle


  getAverageThrottle: (position, throttle, count) ->
    totalThrottle = throttle
    n = 1
    while position.prev? and n < count
      position = position.prev
      break if position.abnormal
      totalThrottle += position.throttle
      n++
    return totalThrottle / n


  getTargetSpeed: (piece, lane) ->
    speed = 0
    if piece.isStrait
      speed = 100
    else
      #speed = @race.track.getMinInvalidSpeedForBendPiece(piece, lane)
      speedLow = @race.track.getMaxValidSpeedForBendPiece(piece, lane)
      speedHigh = @race.track.getMinInvalidSpeedForBendPiece(piece, lane)
      speed = speedLow + 0.3 * (speedHigh-speedLow)
    return Math.max(1,speed)


  getFinalSpeed: (distance, speed, acceleration, throttle) ->
    tics = 100
    while distance > 0 and tics > 0
      distance -= speed
      speed += acceleration
      acceleration = @getNextAcceleration(speed, acceleration, throttle)
      tics = tics - 1
    return speed

  getNextAcceleration: (speed, acceleration, throttle) ->
    if @oracle.isAccelerationOracleReady()
      return acceleration + @oracle.predictAccelerationDelta(speed, acceleration, throttle)
    else
      return acceleration

module.exports = Crasher2
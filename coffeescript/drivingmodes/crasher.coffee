
class Crasher
  constructor: (@race, @oracle) ->
    @crashed = false
    @targetMultiplier = 0.9
    @curveThrottleTargets = []
    for curve,i in @race.curves
      @curveThrottleTargets.push((i + 1)/@race.curves.length)

  getThrottle: ->
    myCar = @race.myCar
    position = myCar.getLastPosition()

    throttle = if position.prev? then position.prev.throttle else 0

    if position.prev? and position.prev.crash
      @race.slippingAngle = if @crashed then Math.max(@race.slippingAngle, Math.abs(position.prev.angle)) else Math.abs(position.prev.angle)
      console.log "slipping angle = #{@race.slippingAngle}"
      @crashed = true
      @curveThrottleTargets = (throttleTarget*@targetMultiplier for throttleTarget in @curveThrottleTargets)
      @targetMultiplier = @targetMultiplier * @targetMultiplier
    else if position.prev? and position.prev.waiting

    else if position.prev? and position.prev.postCrash

    else
      found = false
      for curve,i in @race.curves
        break if found
        for curvePiece in curve
          break if found
          if curvePiece.index > position.piece.index
            throttle = @curveThrottleTargets[i]
            found = true

    throttle = if throttle > 1 then 1 else throttle
    throttle = if throttle < 0 then 0 else throttle

    return throttle

module.exports = Crasher
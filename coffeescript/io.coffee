net = require('net')
util = require('util')
JSONStream = require('JSONStream')

class Io
  constructor: (@serverHost, @serverPort, @botName, @botKey) ->

  registerDataHandler: (@dataHandler) ->

  registerErrorHandler: (@errorHandler) ->

  joinQuickRace: ->
    @client = net.connect(@serverPort, @serverHost, () =>
      Io.log "#{@botName} connected to #{@serverHost}:#{@serverPort}"
      @send { msgType: 'join', data: { name: @botName, key: @botKey }}
    )
    @_setupJsonStream()
    @client.setNoDelay true

  _setupJsonStream: ->
    @jsonStream = @client.pipe(JSONStream.parse())
    @jsonStream.on 'data', (message) =>
      if @dataHandler? then @dataHandler message else Io.log message
    @jsonStream.on 'error', (message) =>
      if @errorHandler? then @errorHandler message else Io.log message

  send: (json) ->
    @client.write JSON.stringify(json) + '\n'

  quit: ->
    @client.end()

  @log: (message) ->
    console.log util.inspect(message, { depth: 1})

module.exports = Io
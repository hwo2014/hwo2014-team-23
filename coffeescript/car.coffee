util = require('util')

class Car
  constructor: (carData) ->
    @[key] = value for own key, value of carData
    @positions = []
    @crashed = false

  getLastPosition: -> return if @positions.length > 0 then @positions[@positions.length-1] else undefined

  addNewPosition: (carPosition) ->
    lastCarPosition = @getLastPosition()
    if lastCarPosition?
      lastCarPosition.next = carPosition
      carPosition.prev = lastCarPosition
    carPosition.speed = calculateSpeed(carPosition)
    carPosition.acceleration = calculateAcceleration(carPosition)
    annotate(carPosition)
    calculateStatistics(carPosition)
    @positions.push(carPosition)
    return carPosition

  calculateSpeed = (carPosition) ->
    # speed = delta distance / delta tick
    speed = 0
    laneDistanceFromCenter = carPosition.lane.distanceFromCenter
    if carPosition.prev?
      distance = 0
      piece = carPosition.prev.piece
      while piece isnt carPosition.piece
        distance += piece.getLaneLength(laneDistanceFromCenter)
        piece = piece.next
      distance += carPosition.piece.getLaneLength(laneDistanceFromCenter)
      distance -= carPosition.prev.x
      distance -= carPosition.piece.getLaneLength(laneDistanceFromCenter) -  carPosition.x
      deltaTick = carPosition.tick - carPosition.prev.tick
      speed = if deltaTick is 0 then 0 else distance / deltaTick
    return speed

  calculateAcceleration = (carPosition) ->
    acceleration = 0
    if carPosition.prev?
      deltaTick = carPosition.tick - carPosition.prev.tick
      deltaSpeed = carPosition.speed - carPosition.prev.speed
      acceleration = if deltaTick is 0 then 0 else deltaSpeed/deltaTick
    return acceleration

  annotate = (carPosition) ->
    # tick 0 is special
    if carPosition.tick is 0 then carPosition.abnormal = true
    if carPosition.prev?
    # after crash --> waiting
      prev = carPosition.prev
      if (prev.crash or prev.waiting) and not (prev.spawn or carPosition.spawn)
        carPosition.abnormal = true
        carPosition.waiting = true
      # after turboStart --> turbo --> until turboEnd
      if (prev.turboStart or prev.turbo) and not (prev.turboEnd or carPosition.turboEnd)
        carPosition.abnormal = true
        carPosition.turbo = true
      if prev.dnf
        carPosition.dnf = true
        carPosition.abnormal = true
      if prev.finish
        carPosition.finish = true
        carPosition.abnormal = true
      prevPosition = carPosition.prev
      peek = 40
      while prevPosition.prev? and peek > 0
        if prevPosition.spawn
          carPosition.abnormal = true
          carPosition.postCrash = true
        if prevPosition.turboEnd
          carPosition.abnormal = true
          carPosition.postTurbo = true
        prevPosition = prevPosition.prev
        peek--


  calculateStatistics = (carPosition) ->
    # if we just got to a new track piece we want to calculate some statistics for the previous track piece
    prevPos = carPosition.prev
    if prevPos?
      prevPiece = prevPos.piece
      if prevPiece isnt carPosition.piece
        maxSpeed = 0
        maxAbsAngle = 0
        abnormal = false
        crash = false
        while prevPos? and prevPos.piece is prevPiece
          abnormal = abnormal or prevPos.abnormal
          crash = crash or prevPos.crash
          maxSpeed = Math.max(maxSpeed, prevPos.speed || 0)
          maxAbsAngle = Math.max(maxAbsAngle, Math.abs(prevPos.angle || 0))
          prevPos = prevPos.prev
        if not abnormal
          prevPiece.maxSpeed = Math.max(maxSpeed, prevPiece.maxSpeed || 0)
          prevPiece.maxAbsAngle = Math.max(maxAbsAngle, prevPiece.maxAbsAngle || 0)
        if crash
          prevPiece.maxSpeedBeforeCrash = Math.max(maxSpeed, prevPiece.maxSpeedBeforeCrash || 0)


  getSpeed: -> return @getLastPosition()?.speed or 0

  getThrottle: -> return @getLastPosition()?.throttle or 0

  setThrottle: (throttle) ->
    position = @getLastPosition()
    if position? then position.throttle = throttle

  getAngle: -> return @getLastPosition()?.angle or 0

  toString: ->
    s = ''
    s += "name: #{@id.name}"
    s += ", color: #{@id.color}"
    s += ", positions: #{@positions.length}"
    return s

module.exports = Car

Car = require('./car')
Track = require('./track')
CarPosition = require('./carposition')


class Race
  constructor: (@botName) ->
    @progress = 0
    @currentLap = 0
    @slippingAngle = 90

  setCars: (carsData) ->
    @cars = (new Car(carData) for carData in carsData)
    @myCar = @getCar(@botName)
    return @cars

  setTrack: (trackData) ->
    @track = new Track(trackData)
    @calculateCurves()

  calculateCurves: ->
    @curves = []
    currentCurve = []
    for piece in @track.pieces
      if piece.isStrait and currentCurve.length > 0
        @curves.push(currentCurve)
        currentCurve = []
      if not piece.isStrait and currentCurve.length > 0 and piece.angle*currentCurve[0].angle < 0
        @curves.push(currentCurve)
        currentCurve = []
      if not piece.isStrait
        currentCurve.push(piece)
    if currentCurve.length > 0
      @curves.push(currentCurve)

  setRaceLaps: (@raceLaps) ->

  setQualifyingDuration: (@qualifyingDuration) ->

  getCar: (name) ->
    cars = (car for car in @cars when car.id.name is name)
    return if cars.length > 0 then cars[0] else undefined

  getCarByColor: (color) ->
    cars = (car for car in @cars when car.id.color is color)
    return if cars.length > 0 then cars[0] else undefined


  addCarPositionData: (carPositionData, tick) ->
    car = @getCar(carPositionData.id.name)
    return car.addNewPosition(@_createCarPosition(carPositionData, tick))

  _createCarPosition: (carPositionData, tick) ->
    pos = carPositionData.piecePosition
    angle = carPositionData.angle
    piece = @track.pieces[pos.pieceIndex]
    x = pos.inPieceDistance
    lane = @track.lanes[pos.lane.startLaneIndex]
    lap = pos.lap
    timestamp = Date.now()
    return new CarPosition(angle,piece,x,lane,lap,tick,timestamp)

  toString: ->
    s = ''
    s += "track\n:#{@track.toString()}\n"
    s += "qualifyingDuration:#{@qualifyingDuration}\n"
    s += "raceLaps: #{@raceLaps}\n"
    s += "cars:\n"
    s += "car: #{car.toString()}\n" for car in @cars
    s += "myCar:#{@myCar.toString()}\n"
    return s

module.exports = Race
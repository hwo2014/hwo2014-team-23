
class CarPosition
  constructor: (@angle, @piece, @x, @lane, @lap, @tick, @timestamp) ->
    @prev = null
    @next = null

  getLaneRadius: ->
    return 0 if @piece.isStrait
    laneOffset = if @piece.angle < 0 then @lane.distanceFromCenter else -@lane.distanceFromCenter
    return @piece.radius + laneOffset

  getLaneAngle: ->
    return 0 if @piece.isStrait
    return @piece.angle

  getAbsLaneAngle: ->
    return Math.abs(@getLaneAngle())

  getLaneLength: ->
    return @piece.getLaneLength(@lane.distanceFromCenter)

  getInPieceRelativeX: ->
    laneLength = @getLaneLength()
    return if laneLength == 0 then 0 else @x / laneLength

  toString: ->
    s = ''
    s += "tick: #{@tick}"
    s += ", lap: #{@lap}"
    s += ", piece: #{@piece.index}"
    #s += ", x:#{toOneDecimal(@x)}"
    s += ", lane: #{@lane.index}"
    if @piece.radius? and @piece.angle? then s += "(r: #{toOneDecimal(@piece.radius)}, a:#{toOneDecimal(@piece.angle)})"
    if @piece.maxSpeed? then s += ", maxSpeed: #{toOneDecimal(@piece.maxSpeed)}"
    if @piece.maxSpeedBeforeCrash? then s += ", maxSpeedBeforeCrash: #{toOneDecimal(@piece.maxSpeedBeforeCrash)}"
    if @piece.maxAbsAngle? then s += ", maxAbsAngle: #{toOneDecimal(@piece.maxAbsAngle)}"
    s += ", angle:#{toOneDecimal(@angle)}"
    s += ", speed: #{toOneDecimal(@speed)}"
    s += ", acceleration: #{toFourDecimal(@acceleration)}"
    if @throttle? then s += ", throttle: #{toOneDecimal(@throttle)}"
    if @abnormal? then s += ", abnormal"
    if @crash? then s += ", crash"
    if @postCrash? then s += ", postCrash"
    if @waiting? then s += ", waiting"
    if @spawn? then s += ", spawn"
    if @turboStart? then s += ", turboStart"
    if @turbo? then s += ", turbo"
    if @turboEnd? then s += ", turboEnd"
    if @finish? then s += ", finish"
    if @dnf? then s += ", dnf"
    return s

  toOneDecimal = (number) ->
    return Math.floor(number*10)/10

  toFourDecimal = (number) ->
    return Math.floor(number*10000)/10000

module.exports = CarPosition
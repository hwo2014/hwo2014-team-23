
TrackPiece = require('./trackpiece')
Lane = require('./lane')


class Track
  constructor: (trackData) ->
    @id = trackData.id

    @lanes = (new Lane(laneData) for laneData in trackData.lanes)
    @lanes.sort((a,b) -> a.index - b.index)
    for lane,i in @lanes
      lane.left = if (i+1) < @lanes.length then @lanes[i+1] else null
      lane.right = if i > 0 then @lanes[i-1] else null

    @pieces = (new TrackPiece(pieceData,index) for pieceData,index in trackData.pieces)
    @length = 0
    for piece,i in @pieces
      piece.next = @pieces[(i+1) % @pieces.length]
      piece.prev = @pieces[(i - 1 + @pieces.length) % @pieces.length]
      piece.trackOffset = @length
      @length += piece.length


  # distance between start of fromPiece to start of toPiece
  calculatePieceDistance: (fromPiece, toPiece, lane) ->
    origFromPiece = fromPiece
    distance = 0
    while fromPiece isnt toPiece and fromPiece.next isnt origFromPiece
      distance += fromPiece.getLaneLength(lane.distanceFromCenter)
      fromPiece = fromPiece.next
    return distance

  getMaxValidSpeedForBendPiece: (trackPiece, lane) ->
    maxRadius = trackPiece.getLaneRadius(lane) + 0.1
    maxSpeed = 0
    for piece in @pieces when not piece.isStrait and piece.getLaneRadius(lane) <= maxRadius
      maxSpeed = Math.max(maxSpeed, piece.maxSpeed || 0)
    return maxSpeed

  getMinInvalidSpeedForBendPiece: (trackPiece, lane) ->
    minRadius = trackPiece.getLaneRadius(lane) - 0.1
    minSpeed = @getMaxSpeedForBendPiece()
    for piece in @pieces when not piece.isStrait and piece.getLaneRadius(lane) >= minRadius and piece.maxSpeedBeforeCrash? and piece.maxSpeedBeforeCrash > 0
      minSpeed = Math.min(minSpeed, piece.maxSpeedBeforeCrash)
    return minSpeed

  getMaxSpeedForBendPiece: ->
    maxSpeed = 0
    for piece in @pieces when not piece.isStrait
      maxSpeed = Math.max(maxSpeed, piece.maxSpeed || 0, piece.maxSpeedBeforeCrash || 0)
    return maxSpeed


  toString: ->
    s = ''
    s += "id:#{@id}\n"
    s += "lanes\n"
    s += "lane: #{lane.toString()}\n" for lane in @lanes
    s += "pieces\n"
    s += "piece: #{piece.toString()}\n" for piece in @pieces
    return s

module.exports = Track
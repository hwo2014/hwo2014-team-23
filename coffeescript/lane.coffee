util = require('util')
_ = require('lodash')

class Lane
  constructor: (laneData) ->
    @[key] = value for own key, value of laneData
    @left = null
    @right = null

  toString: ->
    return util.inspect(_.pick(@, ['distanceFromCenter', 'index']), { depth: 0})

module.exports = Lane
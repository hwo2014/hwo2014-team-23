
q = require('q')
Elm = require('./elm/elm')

class Oracle

  constructor: () ->
    #@angleElm = new Elm()
    @accelerationElm = new Elm()

  quit: ->
    #@angleElm.killPinvChild()
    @accelerationElm.killPinvChild()

  update: (car) ->
    #@updateAngleElm(car)
    @updateAccelerationElm(car)

  isAngleOracleReady: -> return @angleElm.network?

  isAccelerationOracleReady: -> return @accelerationElm.network?

  predictAbsAngleDelta: (speed, laneRadius) ->
    throw new Error('network not ready') if not @angleElm.network?
    input = [speed, laneRadius]
    return @angleElm.calculate(input)

  predictAccelerationDelta: (speed, acceleration, throttle) ->
    throw new Error('network not ready') if not @accelerationElm.network?
    input = [speed, acceleration, throttle]
    return @accelerationElm.calculate(input)

  predictNextAcceleration: (carPosition) ->
    accelerationDelta = @predictAccelerationDelta(carPosition.speed, carPosition.acceleration, carPosition.throttle)
    return carPosition.acceleration + accelerationDelta

  updateAngleElm: (car) ->
    if not @angleElm.processing
      angleFeatureVectors = []
      angleOutputs = []
      validPosition = (position) -> return !position.abnormal? && position.next? && !position.next.abnormal? && !position.piece.isStrait && position.piece == position.next.piece
      for position in car.positions when validPosition(position)
        angleFeatureVectors.push(getAngleFeatureVector(position))
        angleOutputs.push(getAngleOutputValue(position))
      if angleFeatureVectors.length > 50
        hiddenLayerSize = Math.floor(Math.min(100, angleFeatureVectors.length * 0.5))
        @angleElm.createNetwork(angleFeatureVectors, angleOutputs, hiddenLayerSize)

  getAngleFeatureVector = (carPosition) ->
    return [carPosition.speed, carPosition.getLaneRadius()]

  getAngleOutputValue = (carPosition) -> Math.abs(carPosition.next.angle - carPosition.angle)


  updateAccelerationElm: (car) ->
    if not @accelerationElm.processing
      accelerationFeatureVectors = []
      accelerationOutputs = []
      validPosition = (position) -> return !position.abnormal? && position.throttle? && position.next? && !position.next.abnormal?
      for position in car.positions when validPosition(position)
        accelerationFeatureVectors.push(getAccelerationFeatureVector(position))
        accelerationOutputs.push(getAccelerationOutputValue(position))
      if accelerationFeatureVectors.length > 50
        hiddenLayerSize = Math.floor(Math.min(700, accelerationFeatureVectors.length * 0.5))
        @accelerationElm.createNetwork(accelerationFeatureVectors, accelerationOutputs, hiddenLayerSize)

  getAccelerationFeatureVector = (carPosition) ->
    return [carPosition.speed, carPosition.acceleration, carPosition.throttle]

  getAccelerationOutputValue = (carPosition) -> return carPosition.next.acceleration - carPosition.acceleration




module.exports = Oracle




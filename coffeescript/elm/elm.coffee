
# Simple implementation of ELM (Extreme Learning Machine)
# http://www.di.unito.it/~cancelli/retineu11_12/ELM-NC-2006.pdf


la = require('./linearalgebra')
numeric = require('numeric')
jstat = require('jStat').jStat
fork = require('child_process').fork
util = require('util')
#q = require('q')



# numeric.seedrandom.seedrandom(Date.now())
# Math.random = numeric.seedrandom.random



class ELM

  # g: activation function
  constructor: () ->
    @g = (v) -> 1/(1+Math.exp(-v))
    @createPinvChild()

  # X: input matrix (each row == input vector)
  # t: array of output values
  # M: number of hidden nodes
  createNetwork: (X, t, @M) ->
    return if @processing
    @processing = true
    @inputNormalizer = createVectorNormalizer(X, [0,1])   # input vector elements normalized to [0,1]
    outputNormalizer = createNormalizer(t, [-1,1])        # output values normalized to [-1,1]
    @outputDenormalizer = createDenormalizer(t, [-1,1])

    X = normalizeInput(X, @inputNormalizer)
    t = normalizeOutput(t, outputNormalizer)

    @W = generateW(@M, X[0].length)
    @beta = generateBeta(@M)
    H = generateH(X, @W, @beta, X.length, @M, @g)
    @calculateB(H,t)

  killPinvChild: ->
    @pinvChild.send({quit:true})

  createPinvChild: ->
    return if @pinvChild?
    @processing = false
    console.log "forking elm_pinv.js"
    @pinvChild = fork(__dirname + '/elm_pinv.js')
    @pinvChild.on('message', (result) =>
      if result.error?
        console.log 'pinv error: \n' + util.inspect(result.error)
      else
        console.log 'pinv message, duration: \n' + result.duration
        @network = {}
        @network.b = result.b
        @network.inputNormalizer = @inputNormalizer
        @network.outputDenormalizer = @outputDenormalizer
        @network.W  = @W
        @network.beta  = @beta
        @network.M  = @M
      @processing = false
    )
    @pinvChild.on('error', () =>
      console.log 'pinv error!'
      @processing = false
    )

  calculateB: (H,t) ->
    #invH = la.pInv(H)
    #@b = numeric.dot(invH, t)
    console.log "sending message to elm_pinv.js: H rows = #{H.length}, H dimension = #{H[0].length}, t rows = #{t.length}"
    @pinvChild.send({H:H,t:t})

  normalizeInput = (X, normalizer) ->
    X_normalized = []
    for x in X
      X_normalized.push(normalizer(x))
    return X_normalized


  normalizeOutput = (t, normalizer) ->
    t_normalized = []
    for value in t
      t_normalized.push(normalizer(value))
    return t_normalized


  generateW = (M, n) ->
    #return numeric.random([M,n])
    return la.randomMatrix(M,n)

  generateBeta = (M) ->
    #return numeric.random([M,1])
    return la.randomMatrix(M,1)


  generateH = (X, W, beta, N, M, g) ->
    H = new Array(N)
    for i in [0...N]
      H[i] = new Array(M)
      for j in [0...M]
        H[i][j] = g(numeric.dotVV(X[i],W[j]) + beta[j])
    return H


  createNormalizer = (v, interval) ->
    params = getNormalizationParameters(v,interval)
    return (value) ->
      return (value - params.mean) * params.scaling + params.translation


  createDenormalizer = (v, interval) ->
    params = getNormalizationParameters(v,interval)
    return (value) ->
      return (value - params.translation) / params.scaling + params.mean


  getNormalizationParameters = (v, interval) ->
    throw new Error('invalid input') if !(v? && interval?)
    mean = jstat.mean(v)
    v_range = jstat.range(v)
    interval_range = jstat.range(interval)
    scaling = if v_range is 0 then 1 else interval_range/v_range
    translation = jstat.min(interval) - (jstat.min(v)-mean)*scaling
    return {mean:mean, scaling:scaling, translation:translation}


  createVectorNormalizer = (X, interval) ->
    throw new Error('invalid input') if !(X? && interval?)
    X_t = numeric.transpose(X)
    normalizers = []
    for v in X_t
      normalizers.push(createNormalizer(v, interval))
    return (x) ->
      normalized_x = []
      for value,i in x
        normalized_x.push(normalizers[i](value))
      return normalized_x


  calculate: (x) ->
    throw new Error('network not ready') if not @network?
    x = @network.inputNormalizer(x)
    y = new Array(@network.M)
    for i in [0...@network.M]
      y[i] = @g(numeric.dotVV(x,@network.W[i]) + @network.beta[i])
    result = numeric.dotVV(y, @network.b)
    return @network.outputDenormalizer(result)


module.exports = ELM


###

X = []
t = []

f = (x,y,z,v,w) -> 0.00001 * (-4*x*v + 2*y*y - z*z*z + 0.01*w*z + 6*w)

for i in [0...500]
  x1 = Math.random()*0.5 - 0.5
  x2 = Math.random()*3 - 3
  x3 = Math.random()*0.001 - 0.001
  x4 = Math.random()*10
  x5 = Math.random()*-0.5
  X.push([x1,x2,x3,x4,x5])
  t.push(f(x1,x2,x3,x4,x5))

elm = new ELM()

createFoo = (n) ->
  return () -> foo(n)

foo = (n) ->
  if not elm.processing
    if elm.network?
      x1 = Math.random()*0.5 - 0.5
      x2 = Math.random()*3 - 3
      x3 = Math.random()*0.001 - 0.001
      x4 = Math.random()*10
      x5 = Math.random()*-0.5
      y = elm.calculate([x1,x2,x3,x4,x5])
      y_real = f(x1,x2,x3,x4,x5)
      error = 100*Math.abs((y_real-y)/y_real)
      console.log "f(#{x1},#{x2},#{x3},#{x4},#{x5}) = #{y_real} ~ #{y} => #{error}% error"
      n = n-1
    elm.createNetwork(X,t,200)
  if n > 0 then setTimeout(createFoo(n), 50) else elm.killPinvChild()


foo(10)

###
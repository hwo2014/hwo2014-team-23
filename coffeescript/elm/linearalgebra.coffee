

numeric = require('numeric')    # http://www.numericjs.com/index.php
jstat = require('jStat').jStat  # http://jstat.github.io/index.html



class LinearAlgebra
  @pInv: (A) ->
    z = numeric.svd(A)
    U = z.U
    S = z.S
    V = z.V
    m = A.length      # rows
    n = A[0].length   # columns
    tol = Math.max(m,n)*numeric.epsilon*S[0]
    M = S.length      # rows in S
    Sinv = new Array(M)
    for i in [M-1..0]
      Sinv[i] = if (S[i] > tol) then 1/S[i] else 0
    return numeric.dot(numeric.dot(V,numeric.diag(Sinv)),numeric.transpose(U))

  @randomMatrix: (rows, columns) ->
    M = new Array(rows)
    for i in [0...rows]
      if columns is 1
        M[i] = 2*Math.random() - 1
      else
        M[i] = new Array(columns)
        for j in [0...columns]
          M[i][j] = 2*Math.random() - 1


module.exports = LinearAlgebra

#!/usr/bin/env node

la = require('./linearalgebra')
numeric = require('numeric')

process.on('message', (data) ->
  process.exit(0) if data.quit?

  start = Date.now()

  try
    invH = la.pInv(data.H)
    b = numeric.dot(invH, data.t)
  catch error
    process.send({error:error})
    return

  duration = Date.now() - start
  process.send({b:b, duration:duration})
)

util = require('util')
Io = require('./io')
Race = require('./race')
Oracle = require('./oracle')
Constant = require('./drivingmodes/constant')
Scout = require('./drivingmodes/scout')
Crasher2 = require('./drivingmodes/crasher2')


serverHost = process.argv[2] || 'senna.helloworldopen.com'  #Testserver with constant physics: prost.helloworldopen.com
serverPort = process.argv[3] || 8091
botName = process.argv[4] || 'sinuski'
botKey = process.argv[5] || 'SL884DmRh/B9PA'




race = new Race(botName)
oracle = new Oracle()
io = new Io(serverHost, serverPort, botName, botKey)
drivingMode = new Constant()


dataHandler = (message) =>
  handler = handlers[message.msgType];
  if handler?
    handler(message)
  else
    Io.log "unknown msgType: #{message.msgType}"
    io.send {msgType: 'ping', data: {}}

errorHandler = (message) =>
  Io.log message


io.registerDataHandler(dataHandler)
io.registerErrorHandler(errorHandler)

maxAccelerationPredictionError = 0
maxAnglePredictionError = 0


handlers = {
  'yourCar': (message) ->
    console.log 'yourCar'
    console.log util.inspect(message, {depth: 1})

  'gameInit': (message) ->
    console.log 'gameInit'
    race.isRace = message.data.race.raceSession.laps? and not message.data.race.raceSession.quickRace
    race.isQuickRace = message.data.race.raceSession.quickRace
    if race.isRace
      race.setRaceLaps(message.data.race.raceSession.laps)
    else
      race.setTrack(message.data.race.track)
      race.setCars(message.data.race.cars)
      race.setQualifyingDuration(message.data.race.raceSession.durationMs)
    console.log race.toString()

  'gameStart': (message) ->
    console.log 'gameStart'
    console.log util.inspect(message, {depth: 1})
    if race.isRace
      drivingMode = new Scout(race, oracle)
      console.log 'new driver: Scout'
    else
      drivingMode = new Crasher2(race, oracle)
      console.log 'new driver: Crasher'

  'carPositions': (message) ->
    #Io.log 'carPositions'

    if race.isQuickRace and race.myCar.getLastPosition()? then console.log race.myCar.getLastPosition().toString()

    for carPositionData in message.data
      tick = message.gameTick || 0
      carPosition = race.addCarPositionData(carPositionData, tick)

    myPosition = race.myCar.getLastPosition()
    myPosition.throttle = drivingMode.getThrottle()

    turboUsed = false
    if race.myCar.turboAvailable
      tics = race.myCar.turboData.turboFactor * race.myCar.turboData.turboDurationTicks + 40
      distance = tics * myPosition.speed
      piece = myPosition.piece
      while piece.isStrait and distance > 0
        distance -= piece.length
        piece = piece.next
      if distance <= 0
        io.send {msgType: 'turbo', data: 'jiihaa!'}
        turboUsed = true
        race.myCar.turboAvailable = false

    if not turboUsed
      io.send {msgType: 'throttle', data: myPosition.throttle}

    if myPosition.lap > 0
      oracle.update(race.myCar)

    #if oracle.isAngleOracleReady() and !myPosition.piece.isStrait
    #  myPosition.nextAbsAngleDeltaPrediction = oracle.predictAbsAngleDelta(myPosition.speed, myPosition.getLaneRadius())
    if oracle.isAccelerationOracleReady()
      myPosition.nextAccelerationPrediction = oracle.predictNextAcceleration(myPosition)

    if myPosition.prev? and myPosition.prev.nextAccelerationPrediction? and myPosition.acceleration != 0
      myPosition.accelerationPredictionError = Math.abs((myPosition.acceleration - myPosition.prev.nextAccelerationPrediction) / myPosition.acceleration) * 100
      console.log "acc: #{Math.floor(myPosition.accelerationPredictionError)}%"
      if myPosition.accelerationPredictionError > maxAccelerationPredictionError
        maxAccelerationPredictionError = myPosition.accelerationPredictionError
        console.log "new max acceleration prediction error: #{maxAccelerationPredictionError}"

    ###
    if myPosition.prev? and myPosition.prev.nextAbsAngleDeltaPrediction? and myPosition.prev.nextAbsAngleDeltaPrediction > 0
      myPosition.anglePredictionError = Math.abs(myPosition.angle - myPosition.prev.angle) / myPosition.prev.nextAbsAngleDeltaPrediction * 100
      console.log "ang: #{Math.floor(myPosition.anglePredictionError)}%"
      if myPosition.anglePredictionError > maxAnglePredictionError
        maxAnglePredictionError = myPosition.anglePredictionError
        console.log "new max angle prediction error: #{maxAnglePredictionError}%"
    ###

  'lapFinished': (message) ->
    race.currentLap = message.data.lapTime.lap
    car = race.getCarByColor(message.data.car.color)
    if car? and car is race.myCar and not race.isRace and message.data.lapTime.lap is 0
      drivingMode = new Scout(race, oracle)
      console.log 'new driver: Scout'
    console.log 'lapFinished'
    console.log util.inspect(message, {depth: 1})

  'gameEnd': (message) ->
    console.log 'gameEnd'
    console.log util.inspect(message, {depth: 1})

  'tournamentEnd': (message) ->
    console.log 'tournamentEnd'
    console.log util.inspect(message, {depth: 1})
    io.quit()
    oracle.quit()
    process.exit 0

  'crash': (message) ->
    car = race.getCarByColor(message.data.color)
    if car
      lastPosition = car.getLastPosition()
      lastPosition.abnormal = true
      lastPosition.crash = true
      console.log "crash: #{message.data.name}"

  'spawn': (message) ->
    car = race.getCarByColor(message.data.color)
    if car
      lastPosition = car.getLastPosition()
      lastPosition.abnormal = true
      lastPosition.spawn = true
      console.log "spawn: #{message.data.name}"

  'turboAvailable': (message) ->
    for car in race.cars
      if not car.waiting
        car.turboAvailable = true
        car.turboData = message.data
    console.log "turboAvailable"

  'turboStart': (message) ->
    car = race.getCarByColor(message.data.color)
    if car
      car.turboAvailable = false
      lastPosition = car.getLastPosition()
      lastPosition.abnormal = true
      lastPosition.turboStart = true
      console.log "turboStart: #{message.data.name}"

  'turboEnd': (message) ->
    car = race.getCarByColor(message.data.color)
    if car
      lastPosition = car.getLastPosition()
      lastPosition.abnormal = true
      lastPosition.turboEnd = true
      console.log "turboEnd: #{message.data.name}"


  'dnf': (message) ->
    car = race.getCarByColor(message.data.car.color)
    if car
      lastPosition = car.getLastPosition()
      lastPosition.abnormal = true
      lastPosition.dnf = true
      console.log "dnf: #{message.data.car.name}, reason: #{message.data.reason}"


  'finish': (message) ->
    car = race.getCarByColor(message.data.color)
    if car
      lastPosition = car.getLastPosition()
      lastPosition.abnormal = true
      lastPosition.finish = true
      console.log "finish: #{message.data.name}"
}


io.joinQuickRace()


process.on('uncaughtException', Io.log)
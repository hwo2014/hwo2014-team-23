util = require('util')
_ = require('lodash')

class TrackPiece
  constructor: (pieceData, index) ->
    @[key] = value for own key, value of pieceData
    @hasSwitch = !!pieceData['switch']
    @isStrait = pieceData.length?
    @length = TrackPiece.calculateBendLaneLength(@radius, @angle) if not @isStrait
    @index = index
    @prev = null
    @next = null

  getLaneLength: (laneDistanceFromCenter = 0) ->
    if @isStrait
      return @length
    else
      laneOffset = if @angle < 0 then laneDistanceFromCenter else -laneDistanceFromCenter
      return TrackPiece.calculateBendLaneLength(@radius + laneOffset, @angle)

  getLaneRadius: (lane) ->
    if @isStrait
      return 0
    else
      laneOffset = if @angle < 0 then lane.distanceFromCenter else -lane.distanceFromCenter
      return @radius + laneOffset


  @calculateBendLaneLength: (radius, angle) ->
    return Math.abs(2.0 * Math.PI * radius * (angle / 360.0))

  toString: ->
    return util.inspect(_.pick(@, ['length', 'hasSwitch', 'index', 'angle', 'radius', 'maxSpeed', 'maxSpeedBeforeCrash', 'trackOffset']), { depth: 0})

module.exports = TrackPiece